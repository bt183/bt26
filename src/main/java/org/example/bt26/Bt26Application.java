package org.example.bt26;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bt26Application {

    public static void main(String[] args) {
        SpringApplication.run(Bt26Application.class, args);
    }

}
