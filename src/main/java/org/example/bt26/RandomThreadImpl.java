package org.example.bt26;

import java.util.Random;
import java.util.Scanner;

public class RandomThreadImpl implements Runnable {
    private int number;
    private final Random random = new Random();
    private final Scanner scanner = new Scanner(System.in);

    public  RandomThreadImpl(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            int randomNumber = this.random.nextInt(100) + 1;
            System.out.println("Đây là số tôi dự đoán: " + randomNumber);

            if (randomNumber == this.number) {
                System.out.println("Có vẻ tôi đoán đúng rồi! Mời bạn nhập lại để tôi đoán tiếp: ");
                System.out.println("Nếu bạn không muốn chơi tiếp thì mời nhập số 111: ");
                int num = scanner.nextInt();
                if (num == 111) {
                    System.out.println("Stopping");
                    break; // Dừng luồng khi người dùng nhập số 111
                }
                System.out.println("Ok vậy tôi tiếp tục dự đoán, bạn đợi tôi vài giây suy nghĩ nhé: ");
                this.number = num;
            }
            System.out.println("Có vẻ không đúng rồi bạn đợi tôi đoán lại nhé!");
            try {
                Thread.sleep(500); // Sleep for 5 seconds
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt(); // Restore interrupted status
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Mời bạn nhập số mà bạn muốn tôi dự đoán: ");
        int number = scanner.nextInt();

        RandomThreadImpl randomThread = new RandomThreadImpl(number);
        Thread thread = new Thread(randomThread); // Tạo một Thread mới và truyền Runnable vào
        thread.start();
    }
}
